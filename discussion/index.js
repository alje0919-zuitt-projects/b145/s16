console.log('Hello World');

//SECTION 1: Assignment Operators
	// 1. Basic assignment operator (=)
let assignmentNumber = 5;
let message = 'This is the message';
	// 2. Addition assignment operator (+ and =)

console.log("Result of the operation: " + assignmentNumber);
//long hand for line 11 -> assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log("Result of the operation: " + assignmentNumber);
	
	//SECTION 1 sub 2
assignmentNumber -= 3
console.log("Result of the operation is: " + assignmentNumber);

//SECTION 2: Arithmetic Operators
let x = 15;
let y = 10;

//addition (+)
let sum = x + y;
console.log(sum);

//subtraction (-);
let diff = x - y;
console.log(diff);

//multiplication (*)
let product = x * y;
console.log(product);

//division (/)
let quotient = x / y;
console.log(quotient);

//remainder between the 2 values (Modulus '%')
let remainder = x % y;
console.log(remainder);

	//SUBSECTION - Multiple Operators and Parentheses
	//follows PEMDAS rule (Parenthesis Exponent Multiplication Division Addition Subtraction)
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 -3) * (4 / 5);
console.log(pemdas);

//INCREMENT and DECREMENT
let z = 1;
	//Pre and Post
	//Increment (++)
//pre-increment (syntax: ++variable)
let preIncrement = ++z; // 1 + 1
console.log(preIncrement);
//the value of Z was increased
console.log(z);

//post-increment (syntax: varilable++)
postIncrement = z++;
console.log(postIncrement);
//z + 1
//the value of z is returned and stored inside the variable called "PostIncrement". the value of z is at 2 before it was incremented.
console.log(z);
// 1 + z vs z + 1

//DECREMENT (--)
let preDecrement = --z;
console.log(preDecrement);

//Post-decrement (syntax: variableName--)
let postDecrement = z--;
console.log(postDecrement);
console.log(z);


//SECTION; TYPE COERCION
let numberA = 6;
let numberB = '6';

//check data type of values using typeof
console.log(typeof numberA);
console.log(typeof numberB);

let coercion = numberA + numberB;
console.log(coercion);

console.log(typeof coercion); // it became string

//adding number and boolean
let expressionC = 10 + true;
console.log(expressionC);
console.log(typeof expressionC);

//Number with a Null value
let expressionG = 8 + null;
console.log(expressionG);

/*CONVERSION RULES;
	1. If atleast one operand is an OBJECT, it will be converted into a primitive value/data type.
	2. after conversion, if atleast 1 operand is a string data type, the 2nd operand is converted into another string to perform concatenation.
	3. in other cases where both operands are converted to numbers then an arithmetic operation is executed.
	* the object data type which is NULL operand was converted into a primitive data type.
*/

let expressionH = "Batch145" + null;
console.log(expressionH); //Batch145null

//Number with Undefined
expressionH = 9 + undefined; // NaN -> Not a number
console.log(expressionH);

//1. undefined was converted into a number data type NaN
//2. 9 + NaN = NaN

//SECTION - Comparison Operators
let name = 'Juan';

	//Equality Operator (==)
	//returns a boolean value, and attempts to CONVERT and COMPARE operands with 2 different data types.
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(1 == true); //true
	console.log(1 == false); //false
	console.log(name == 'Juan'); //true
	// console.log('Juan' == Juan); // error not defined //

	//Inequality Operator (!=)
		//checks whether the operands are NOT EQUAL/HAVE DIFFERENT VALUES.

	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log(0 != false); //false
	let juan = 'juan';
	console.log('juan' != juan); //false

		/*STRICT Equality Operator (===)
			- checks whether the operands are equal or have the same value.
			- also compares if the data types are the same.
		*/
	console.log(1 === 1); //true
	console.log(1 === '1'); //false
	console.log(0 === false); //false
	//they have different data types hence, false.

		/*STRICK Inequality Operator (!==)
			- checks if operands are not equal or have different values/content.
			- checks both values and data types of both components or operands.
		*/
	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== '1'); //true
	console.log(0 !== false); //true

/* Developer's tip: upon creating conditions or statement, it is strongly recommended to use "strict" equality operators over "loose" equality operators because it will be easier for us to predetermine outcomes and results in any given scenario. */

//SECTION: RELATIONAL OPERATORS
let priceA = 1800;
let priceB = 1450;
	//lesser than operator
	console.log(priceA < priceB); //false
	//greater than operator
	console.log(priceA > priceB); //true

let expressionI = 150 <= 150;
console.log(expressionI); //true


// LOGICAL OPERATORS
/* Developer's Tip: when writing down/selecting variables name that would describe/contain a boolean value, it is a writing convention for developers to add a prefix of "is" or "are" togehter with the variable name to form a variable simliar on how to answer a simple yes or no question.
	isSingle = true;
*/

isLegalAge = true;
isRegistered = false;
//for the person to be able to vote, both requirements has to be met.
	
	//AND (&& - double ampersand) all criteria has to be met. evaluates whether all are true.

let isAllowedtoVote = isLegalAge && isRegistered;
console.log('Is the person allowed to vote? ' + isAllowedtoVote);

	//OR (|| - double pipe) operator takes two or more evaluations and evaluates whether some are true.
let isVaccinated = isLegalAge || isRegistered;
console.log('Is allowed for vaccination? ' + isVaccinated);

	/*NOT (! - exclamation point)
		- this will convert/return the opposite value */
let isTaken = true;
let isTalented = false;
console.log(!isTaken);
console.log(!isTalented);