console.log('Hello World');

let x = 150;
let y = 100;

let sum = x + y;
console.log('The sum of the two numbers is: ' + sum);

let diff = x - y;
console.log('The difference of the two numbers is: ' + diff);

let prod = x * y;
console.log('The product of the two numbers is: ' + prod);

let quo = x / y;
console.log('The quotient of the two numbers is: ' + quo);

console.log('The sum is greater than the difference: ' + (sum > diff));

let isprodPositive = prod > 0;
let isquoPositive = quo > 0;
console.log('The product and quotient are positive numbers: ' + (isprodPositive && isquoPositive));

let issumNeg = sum < 0;
let isdiffNeg = diff < 0;
let isprodNeg = prod < 0;
let isquoNeg = quo < 0;
console.log('One of the results is negative: ' + (issumNeg || isdiffNeg || isprodNeg || isquoNeg ));

let issumZero = sum == 0;
let isdiffZero = diff == 0;
let isprodZero = prod == 0;
let isquoZero = quo == 0;
console.log('One of the results is zero: ' + (issumZero || isdiffZero || isprodZero || isquoZero ));